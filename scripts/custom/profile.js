$(document).ready(function() {

});

function add_friend(userid) 
{
	$.ajax({
		url: global_base_url + 'profile/add_friend/' + userid,
		type: 'POST',
		data: {
			csrf_test_name : global_hash
		},
		dataType : 'json',
		success: function(msg) {
			// console.log(msg);
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				$('.else_friend').html(msg.message);
				//$('#friend_button_' + userid).addClass("disabled");
			}
		}
	})
}

function edit_album(id) 
{
	$.ajax({
		url: global_base_url + 'profile/edit_album/' + id,
		type: 'GET',
		data: {
		},
		success: function(msg) {
			$('#editModal').modal('show');
			$('#edit-album').html(msg);
		}
	})
}

function edit_image(id) 
{
	$.ajax({
		url: global_base_url + 'profile/edit_image/' + id,
		type: 'GET',
		data: {
		},
		success: function(msg) {
			$('#editModal').modal('show');
			$('#edit-album').html(msg);
		}
	})
}

 $(document).on('click','#friend_cancel',function(){

  var userid=$('#friend_cancel').attr('data-friend');


    if(confirm("Are you sure want to cancel friend request?"))
    {
           //console.log($(this).attr('data-friend'));
           $.ajax({
		url: global_base_url + 'profile/cancel_friend/' + userid,
		type: 'POST',
		data: {
			csrf_test_name : global_hash
		},
		//dataType : 'json',
		success: function(msg) {
			 console.log(msg);
			 $('.else_friend').html(msg);
			// if(msg.error) {
			// 	alert(msg.error_msg);
			// 	return;
			// }
			// if(msg.success) {
			// 	$('.else_friend').html(msg.message);
			// }
		}
	   });
       }
    });
