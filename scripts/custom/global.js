var glang = new Array();
glang.push("Remove Saved Post");
glang.push("Save Post");
glang.push("Turn Off Notifications");
glang.push("Turn On Notifications");




function load_notifications() 
{
	
	$.ajax({
		url: global_base_url + "home/load_notifications",
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		success: function(msg) {
			$('#notifications-scroll').html(msg);
		}

	});
	console.log("Done");
}


var chat_page = 0;
function load_chats() 
{
	
	$.ajax({
		url: global_base_url + "chat/load_chats",
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		success: function(msg) {
			if(msg=='1'){
				$('#chat-scroll').html('');
				$('#chat-msgnew').show();
				$('#chat-click-more').remove();
			}else{
				$('#chat-msgnew').hide();
				$('#chat-scroll').html(msg);	
			}
		}

	});
	console.log("Done");
}

function load_chat_page() 
{
	chat_page += 5;
	$.ajax({
		url: global_base_url + "chat/load_chats/" + chat_page,
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		success: function(msg) {
			
			if(msg=='1'){
				$('#chat-msgnew').show();
				$('#chat-click-more').remove();
			}else{
				$('#chat-msgnew').hide();
				$('#chat-scroll').append(msg);	
			}
			
			
		}

	});
	$.ajax({
		url: global_base_url + "chat/check_notifications",
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		dataType: 'json',
		success: function(msg)
		 {
			  console.log(msg);
			update_chat_noti2(msg.noti_count);
		}
	});
	
	//check_chat_notisasd();
	console.log("Done");
}


function update_chat_noti2(count) 
{
	if (typeof count != "undefined" && count >0) {
		$('#chat-noti').html(count);
		$('#chat-noti').fadeIn();
		if(count %5!=0)
		{
			$('#chat-click-more').hide();
			$('#chat-click-more2').show();
		}
	} else 
	{
		$('#chat-noti').html("");
		$('#chat-noti').fadeOut(10);
		$('#chat-click-more').hide();
			$('#chat-click-more2').show();
	}
}
function load_notifications_unread() 
{
	$.ajax({
		url: global_base_url + "home/load_notifications_unread",
		beforeSend: function () { 
		$('#loading_spinner_notification').fadeIn(10);
		$("#ajspinner_notification").addClass("spin");
	 	},
	 	complete: function () { 
		$('#loading_spinner_notification').fadeOut(10);
		$("#ajspinner_notification").removeClass("spin");
	 	},
		data: {
		},
		success: function(msg) {
			$('#notifications-scroll').html(msg);
			return false;
		}

	});
	console.log("Done");
}


function load_notification_url(id) 
{
	window.location.href= global_base_url + "home/load_notification/" + id;
	return;
}

$(document).ready(function() {
	$("#editor-textarea").mentionsInput({trigger: "#@", source: global_base_url + 'home/get_user_friends'});
	
	$('.dropdown-menu #noti-click-unread').click(function(e) {
	    e.stopPropagation();
	});
	$('#chat-click-more').click(function(e) {
	    e.stopPropagation();
	});
	if(navigator.appVersion.indexOf("Win")!=-1) {
		$('#notifications-scroll,#chat-scroll').niceScroll({touchbehavior: false, zindex: 9999999999});
	}

	$('#search-complete').autocomplete({
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "home/get_search_results",
	             data: {
	             		query : request.term
	             },
	             dataType: 'JSON',
	             success: function (msg) {

	             	 //console.log(msg);
	                 response(msg);
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-avatar"><img src="'+item.avatar+'"></div><div class="search-user-info"><a href="'+item.url+'">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-avatar"><img src="'+item.avatar+'"></div><div class="search-user-info"><a href="'+item.url+'">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });


	$('#with_users').select2({
		placeholder: "Select users",
  		allowClear: true,
  		ajax: {
		    url: global_base_url + "home/get_user_friends_v2",
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      return {
		        term: params.term // search term
		      };
		    }
		},
		minimumInputLength: 1
	});
 
    $('#social-form').submit(function() { 
        
        $(this).ajaxSubmit({
        	success: addPost,
        	dataType: 'json',
        	clearForm: false
        }); 
 
        return false; 
    }); 

    $('#social-form-edit').submit(function() { 
        
        $(this).ajaxSubmit({
        	success: editPostComplete,
        	dataType: 'json',
        	clearForm: true
        }); 
 
        return false; 
    }); 

	$('.map_name').geocomplete();

	$('#home_posts').on("focus", ".feed-comment-input", function() {
		 var dis=$(this);
		var id = $(this).attr("data-id");

		// check to see which events this comment already has
		var events = $._data( this, 'events' ).keypress;	
		
		

        var hasEvents = false;
        for(var i=0;i<events.length;i++) {
        	if(events[i].namespace == "") {
        		hasEvents = true;
        	}
        }

        if(!hasEvents) {
			$(this).keypress(function (e) {
			    if (e.keyCode == 13) { 
			    	 		var form_data = new FormData();
			    	 		if($('.comment_iamge-'+id)[0].files.length)
							{
							  imge=$('.comment_iamge-'+id)[0].files[0];
							  form_data.append('file', imge);
							}

			        var comment = $(this).mentionsInput("getValue");
					$(this).val("");
					dis.val('');
					
					$(this).mentionsInput("clear");
					$('#comment_file-'+id).val('');
						$('#output-'+id).attr("src",'');
						$('#media-'+id).hide();									
						form_data.append('comment', comment);	
						form_data.append('csrf_test_name', global_hash);	
						form_data.append('page', global_page);	
						form_data.append('hide_prev', hide_prev);
			        $.ajax({
						url: global_base_url + 'feed/post_comment/' + id,
						type: 'POST',
						data: form_data,
						dataType: 'json',
						 processData: false,
                         contentType: false,
						success: function(msg) {

							if(msg.error) {
								alert(msg.error_msg);
								return;
							}
							$('#feed-comments-spot-'+id).html(msg.content);
							$('#feed-comments-'+id).html(msg.comments);
						}
					});
			    }
			});
		}
	});


	$(document).on('click','.enter_key',function(){

		//data.append('file', $('#file')[0].files[0]);
		

		var d1=$(this);
		var id= $(this).attr('data-id');  
		var form_data = new FormData();
		
		if($('.comment_iamge-'+id)[0].files.length)
		{
		  imge=$('.comment_iamge-'+id)[0].files[0];
		  form_data.append('file', imge);
		}

		
       var dis=$(this).closest('.feed-comment-wrapper').find('#enter_key-'+id).val();
  
	  if(dis!='')
	  {
		var comment = $(this).closest('.feed-comment-wrapper').find('#enter_key-'+id).val();
		  d1.closest('.feed-comment-wrapper').find('#enter_key-'+id).val('')
		  $('#comment_file-'+id).val('');
		  $('#output-'+id).attr("src",'');
		  $('#media-'+id).hide();	
					
		  form_data.append('comment', comment);	
		   form_data.append('csrf_test_name', global_hash);	
		  form_data.append('page', global_page);	
		  form_data.append('hide_prev', hide_prev);	
					console.log(form_data);
					 

			        $.ajax({
						url: global_base_url + 'feed/post_comment/' + id,
						type: 'POST',
						data: form_data,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function(msg) {

							if(msg.error) {
								alert(msg.error_msg);
								return;
							}
							$('#feed-comments-spot-'+id).html(msg.content);
							$('#feed-comments-'+id).html(msg.comments);
						}
					});
	  }
	  else{
		  alert('Comment cannot be empty!')
	  }
   
});

	$('#home_posts').on("focus", ".feed-comment-input-reply", function() {
		 
		var id = $(this).attr("data-id");

		// check to see which events this comment already has
		var events = $._data( this, 'events' ).keypress;

		

        var hasEvents = false;
        for(var i=0;i<events.length;i++) {
        	if(events[i].namespace == "") {
        		hasEvents = true;
        	}
        }
        console.log(hasEvents);
        if(!hasEvents) {
			$(this).keypress(function (e) {
			    if (e.keyCode == 13) {
			    	 var form_data = new FormData();		
					if($('.comment_iamge1-'+id)[0].files.length)
					{
					  imge=$('.comment_iamge1-'+id)[0].files[0];
					  form_data.append('file', imge);
					}
						        var comment = $(this).mentionsInput("getValue");
			        $(this).val("");
					$(this).mentionsInput("clear");
					$('.comment_iamge1-'+id).val('');
					$('#output1-'+id).attr("src",'');	
					$('#media1-'+id).hide();							  
					form_data.append('comment', comment);	
					 form_data.append('csrf_test_name', global_hash);	
					form_data.append('page', global_page);	
					form_data.append('hide_prev', hide_prev);	
			        $.ajax({
						url: global_base_url + 'feed/post_comment_reply/' + id,
						type: 'POST',
						data: form_data,
						dataType: 'json',
						processData: false,
                         contentType: false,
						success: function(msg) {

							if(msg.error) {
								alert(msg.error_msg);
								return;
							}
							$('#feed-comments-spot-reply-'+id).html(msg.content);
							$('#feed-reply-comments-'+id).html("(" + msg.comments + ")");
							$('#feed-comments-'+msg.feeditemid).html(msg.comments_count);
						}
					});
			    }
			});
		}
	});


});

$(document).on('click','.feed-comment_reply_post',function(){
	var d1=$(this);
	var id= $(this).attr('data-id');     
   var dis=$(this).closest('.feed-comment-wrapper').find('#repaly-commet_post-'+id).val();
 
   var form_data = new FormData();
		
   if($('.comment_iamge1-'+id)[0].files.length)
   {
	 imge=$('.comment_iamge1-'+id)[0].files[0];
	 form_data.append('file', imge);
   }
  if(dis!='')
  {
	var comment = d1.closest('.feed-comment-wrapper').find('#repaly-commet_post-'+id).val();
	d1.closest('.feed-comment-wrapper').find('#repaly-commet_post-'+id).val('')
	//$(this).mentionsInput("clear");

	$('.comment_iamge1-'+id).val('');
		  $('#output1-'+id).attr("src",'');
		  $('#media1-'+id).hide();	
					
		  form_data.append('comment', comment);	
		   form_data.append('csrf_test_name', global_hash);	
		  form_data.append('page', global_page);	
		  form_data.append('hide_prev', hide_prev);	
	$.ajax({
		url: global_base_url + 'feed/post_comment_reply/' + id,
		type: 'POST',
		data: form_data,
		cache: false,
		contentType: false,
		processData: false,
		dataType: 'json',
		success: function(msg) {

			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			$('#feed-comments-spot-reply-'+id).html(msg.content);
			$('#feed-reply-comments-'+id).html("(" + msg.comments + ")");
			$('#feed-comments-'+msg.feeditemid).html(msg.comments_count);
		}
	});
  }
  else{
	   alert('Comment cannot be empty!');
  }
});
  

function editPostComplete(data) 
{
	$('#editPostModal').modal('hide');
	$('#feed-post-'+data.id).replaceWith(data.post);
}

function reloadPost(postid) 
{
	$.ajax({
		url: global_base_url + 'feed/reload_post/' + postid,
		type: 'GET',
		data: {
		},
		dataType : 'json',
		success: function(data) {
			$('#feed-post-'+data.id).replaceWith(data.post);
		}
	});
}

function addPost(msg) 
{
	$('#poll-button').removeClass("highlight-button");
	$('#user-button').removeClass("highlight-button");
	$('#map-button').removeClass("highlight-button");
	$('#video-button').removeClass("highlight-button");
	$('#image-button').removeClass("highlight-button");
	if(msg.error) {
		alert(msg.error_msg);
		return;
	}
	$('#social-form').clearForm();
	$('#editor-textarea').mentionsInput("clear");
	// reload feed
	load_posts_wrapper();
}

var global_page = 0;
var hide_prev = 0;

function load_previous_comments(id, page, obj) 
{
	$(obj).remove();
	$.ajax({
		url: global_base_url + 'feed/get_previous_comments/' + id,
		type: 'GET',
		data: {
			page : page
		},
		success: function(msg) {
			global_page = page;
			hide_prev = 1;
			$('#feed-comment-'+id).prepend(msg);
		}
	});
}

function get_post_likes(id)
{
	$.ajax({
		url: global_base_url + 'feed/get_post_likes/' + id,
		type: 'GET',
		data: {

		},
		success: function(msg) {
			$('#likeModal').modal('show');
			$('#likeModal').find('#myModalLabel').html('<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Post Likes</h4>');
			$('#post-likes').html(msg);
		}
	})
}

function get_post_comments_likes(id)
{
	$.ajax({
		url: global_base_url + 'feed/get_post_comments_likes/' + id,
		type: 'GET',
		data: {

		},
		success: function(msg) {
			$('#likeModal').modal('show');
			$('#likeModal').find('#myModalLabel').html('<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Comment Likes</h4>');
			$('#post-likes').html(msg);
		}
	})
}
function load_comments(id)
{
	if($('#feed-comment-'+id).is(':visible')) {
		$('#feed-comment-'+id).slideUp(400);
	} else {
		$(".feed-comment-input").mentionsInput("destroy");
		$.ajax({
			url: global_base_url + 'feed/get_feed_comments/' + id,
			type: 'GET',
			data: {

			},
			success: function(msg) {
				var chkjson = IsJsonString(msg);
				if(chkjson == true){
					var response = JSON.parse(msg);
					alert(response.error_msg);
				}else{
					$('#feed-comment-'+id).html(msg);
					$('#feed-comment-'+id).slideDown(400);
					$(".feed-comment-input").mentionsInput({source: global_base_url + 'home/get_user_friends'});
				}
				

			}
		});
	}
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function load_single_comment(id, commentid, replyid) 
{
	if($('#feed-comment-'+id).is(':visible')) {
		$('#feed-comment-'+id).slideUp(400);
	} else {
		$(".feed-comment-input").mentionsInput("destroy");
		$.ajax({
			url: global_base_url + 'feed/get_single_comment/' + id,
			type: 'GET',
			data: {
				commentid : commentid
			},
			success: function(msg) {
				$('#feed-comment-'+id).html(msg);
				$('#feed-comment-'+id).slideDown(400);
				$(".feed-comment-input").mentionsInput({source: global_base_url + 'home/get_user_friends'});
				if(replyid >0) {
					load_comment_replies(commentid, replyid);
				}
			}
		});
	}
}


function delete_comment(id) 
{
	$.ajax({
		url: global_base_url + 'feed/delete_feed_comment/' + id + '/' + global_hash,
		type: 'GET',
		data: {

		},
		dataType: 'json',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				$('#feed-comment-area-'+id).fadeOut(500);

				var old=parseInt($('#feed-comments-'+msg.post_id).text());
				 var newval=parseInt(old-1);
				$('#feed-comments-'+msg.post_id).text(newval);

			}
		}
	})
}

function delete_comment_reply(id) 
{
	$.ajax({
		url: global_base_url + 'feed/delete_feed_comment_reply/' + id + '/' + global_hash,
		type: 'GET',
		data: {

		},
		dataType: 'json',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {

				$('#comment-reply-'+id).fadeOut(500);
				 if(msg.count){
			      	$('#feed-reply-comments-'+msg.id).html("(" + msg.count + ")");
				 }

			}
		}
	})
}

function load_comment_replies(id, replyid=0) 
{
	$(".feed-comment-input-reply").mentionsInput("destroy");
	$.ajax({
		url: global_base_url + 'feed/get_feed_comments_replies/' + id,
		type: 'GET',
		data: {

		},
		success: function(msg) {
			$('#feed-comment-reply-'+id).html(msg);
			$('#feed-comment-reply-'+id).slideDown(400);
			$(".feed-comment-input-reply").mentionsInput({source: global_base_url + 'home/get_user_friends'});
			if(replyid > 0) {
				window.location.hash = '#comment-reply-' + replyid;
			}
		}
	})
}
function like_feed_post(id) 
{
	$.ajax({
		url: global_base_url + 'feed/like_post/' + id,
		type: 'GET',
		data: {
			hash : global_hash
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.like_status) {
				$('#likes-click-' + id).fadeIn(10);
				$('#like-button-' +id).addClass("active-like");
			} else {
				$('#like-button-' +id).removeClass("active-like");
			}
			$('#feed-likes-' +id).html(msg.likes);
			load_new_likes(id);

			
		}
	})
	 
	
}
 function load_new_likes(id)
 { 
	$.ajax({
		url: global_base_url + 'feed/load_new_likes/' + id,
		type: 'GET',
		data: {
			hash : global_hash
		},
		success: function(msg) { 
			 console.log(msg);
			 $('.feed-ajax-'+id).html(msg);			
		}
	});
 }

function like_comment(id) 
{
	$.ajax({
		url: global_base_url + 'feed/like_comment/' + id,
		type: 'GET',
		data: {
			hash : global_hash
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.like_status) {
				$('#comment-like-link-' +id).addClass("active-comment-like");
			} else {
				$('#comment-like-link-' +id).removeClass("active-comment-like");
			}
			var like_icon = '';
			if(msg.likes > 0) {
				like_icon = '<span class="glyphicon glyphicon-thumbs-up" id=""></span> ' + msg.likes;
			}
			$('#comment-like-' +id).html(like_icon);
			load_new_comment_likes(id);
		}
	})
}

function load_new_comment_likes(id)
 { 
	$.ajax({
		url: global_base_url + 'feed/load_new_comment_likes/' + id,
		type: 'GET',
		data: {
			hash : global_hash
		},
		success: function(msg) { 
			 console.log(msg);
			 $('.feed-ajax-comment-like-'+id).html(msg);			
		}
	});
 }

function promote_post(id)
{
	$.ajax({
		url: global_base_url + 'feed/promote_post/' + id,
		type: 'GET',
		data: {
		},
		success: function(msg) {
			// Load modal
			$('#promotePost').html(msg);

			$('#promotePostModal').modal('show');
		}
	});
}

function vote_poll(postid, type) 
{
	// Answers
	if(type == 0) {
		var answers = $('#poll_answers_' + postid + ' input[type=radio]:checked').val();
	} else {
		var searchIDs = $('#poll_answers_' + postid + ' input[type=checkbox]:checked').map(function(){

	      return $(this).val();

	    });
		var answers =searchIDs.get();
	}

	$.ajax({
		url: global_base_url + 'feed/vote_poll/' + postid,
		type: 'GET',
		data: {
			answers : answers
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				// Reload post
				reloadPost(postid);
			}
		}
	});
}

function edit_post(id)
{
	$.ajax({
		url: global_base_url + 'feed/edit_post/' + id,
		type: 'GET',
		data: {
		},
		success: function(msg) {
			// Load modal
			$('#editPost').html(msg);
			//$("#editor-textarea").mentionsInput("destroy");
			$(".edit-editor-textarea").mentionsInput({trigger: "#@", source: global_base_url + 'home/get_user_friends'});
			$('#editPostModal').modal('show');

			$('#edit-image').click(function() {
				$('#edit-image-area').toggle();
			});

			$('#edit-video').click(function() {
				$('#edit-video-area').toggle();
			});

			$('#edit-location').click(function() {
				$('#edit-location-area').toggle();
			});

			$('#edit-users').click(function() {
				$('#edit-users-area').toggle();
			});

			$('#edit-poll').click(function() {
				$('#edit-poll-area').toggle();
			});

			$('.map_name').geocomplete();

			$('#social-form-edit').submit(function() { 
        
		        $(this).ajaxSubmit({
		        	success: editPostComplete,
		        	dataType: 'json',
		        	clearForm: true
		        }); 
		 
		        return false; 
		    }); 

		    $('.with_users').select2({
				placeholder: "Select users",
		  		allowClear: true,
		  		ajax: {
				    url: global_base_url + "home/get_user_friends_v2",
				    dataType: 'json',
				    delay: 250,
				    data: function (params) {
				      return {
				        term: params.term // search term
				      };
				    }
				},
				minimumInputLength: 1
			});
		}
	})
}

function delete_post(id) 
{
	$.ajax({
		url: global_base_url + 'feed/delete_post/' + id + '/' + global_hash,
		type: 'GET',
		data: {
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				$('#feed-post-' + id).fadeOut(500);
				$('.post_count').text(msg.post_count);
			}
		}
	})
}

function share_post1(id) 
{
	 $('.confirm_share').attr('data-post_id',id);
		$('#share_confirm_post').modal('show');
}

$(document).on('click','.confirm_share',function(){
	share_post($(this).attr('data-post_id'))
});

function share_post(id) 
{
	$.ajax({
		url: global_base_url + 'feed/share_post/' + id + '/' + global_hash,
		type: 'GET',
		data: {
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				// reload feed
				load_posts_wrapper();
			}
		}
	})
}

var share_post_id = '';
function share_post_in_chat(id){
	$('.confirm_share_chat').attr('data-post_id',id);
	share_post_id = 'share_friend_'+id;
	$('.share-to-friend').attr('id','share_friend_'+id);;
	$('#share_post_chat_model').modal('show');
}	
$(document).on("click", ".confirm_share_chat", function(event){
	var post_id = $(this).attr('data-post_id');
	var friend = $('#'+share_post_id).val();
	if(friend=='')
	{ 
	   $('.error-share').show().text('Please Enter Friend name');
	   return false;
	}else{
		// var username = 
		$.ajax({
			url: global_base_url + 'feed/share_post_in_chat/' + post_id +'/'+ friend + '/' + global_hash,
			type: 'get',
			data: {
			},
			dataType: 'JSON',
			success: function(msg) {
				if(msg.status == 1) {
					$('#share_post_chat_model').modal('hide');
					$('#share_post_chat_model').on('hidden.bs.modal', function () {
						alert(msg.response);
					});
				}if(msg.error==1){
					$('.error-share').show().html('<strong>Error : </strong>'+ msg.error_msg);
					return;
				}
				
			}
		});	
	}
});
// console.log(share_post_id);//
$(document).on('keydown','.share-to-friend', function(){
	$('.error-share').hide().text();
	var post_id = $(this).attr('id');
$('#'+post_id).autocomplete({
    delay : 300,
    minLength: 3,
    source: function (request, response) {
        // console.log(response); return false;
         $.ajax({
             type: "GET",
             url: global_base_url + "home/get_chat_user_friends_v2",
             data: {
                term : request.term

             },
             dataType: 'JSON',
             success: function (msg) {
                if(!msg.length){
                  $('#local_error').show().html(' <strong>Error</strong> Please enter any friend name.').fadeOut(3000);                  
                }
                 response(msg);
             }
         });
      }
  });
});


function save_post(id) 
{
	$.ajax({
		url: global_base_url + 'feed/save_post/' + id + '/' + global_hash,
		type: 'GET',
		data: {
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				if(msg.status == 1) {
					$('#save_post_' + id).html(lang[0]);
				} else {
					$('#save_post_' + id).html(lang[1]);
				}
			}
		}
	})
}

function subscribe_post(id) 
{
	$.ajax({
		url: global_base_url + 'feed/subscribe_post/' + id + '/' + global_hash,
		type: 'GET',
		data: {
		},
		dataType: 'JSON',
		success: function(msg) {
			if(msg.error) {
				alert(msg.error_msg);
				return;
			}
			if(msg.success) {
				if(msg.status == 1) {
					$('#subscribe_post_' + id).html(lang[2]);
				} else {
					$('#subscribe_post_' + id).html(lang[3]);
				}
			}
		}
	})
}

function set_post_as(id, img) 
{
	$('#post_as').val(id);
	$('#editor-poster-icon').attr("src", img);
	$('.postastoggle').toggle();
	$('.postastoggle').removeClass("postastoggle");
	$('#'+id+'-postas').addClass("postastoggle");
	$('#' + id+'-postas').fadeOut();
}

function add_smile($text) { 

	setTimeout(function(){ $('#eid_footer').addClass('open'); }, 1);

	
	$('#editor-textarea').val($('#editor-textarea').val() + " " +$text);
	$('input[name="content"]').val($('input[name="content"]').val() + " " +$text);
	$('#Smilies').attr('aria-expanded',"true");
	$('#smilesid').show();

}

function edit_smile($text) {
	$('.edit-editor-textarea').val($('.edit-editor-textarea').val() + " " +$text);
	$('.edit-editor-textarea').trigger('change');
}


function add_chat_smile($text) { 

	setTimeout(function(){ $('#smiles-dropdown-wrapper').addClass('open'); }, 1);

	
	$('#mail-reply-textarea').val($('#mail-reply-textarea').val() + " " +$text);
	// $('#smiles-dropdown-wrapper').addClass('open');
	$('#chat-smilies').attr('aria-expanded','true');
	$('#chat-smilesid').show();

}

//   var loadFile = function(event,$id) {
//     var output = document.getElementById('output');
//     output.src = URL.createObjectURL(event.target.files[0]);
//   };

function loadFile(event,id)
{
	// alert(',media-'+id);
	$('#media-'+id).show();
	//document.getElementById("media").style.display = "none"; 
  var output = document.getElementById('output-'+id);
   output.src = URL.createObjectURL(event.target.files[0]);

   $('#enter_key-'+id).focus();
}
function loadFile1(event,id)
{
	$('#media1-'+id).show();
  var output = document.getElementById('output1-'+id);
   output.src = URL.createObjectURL(event.target.files[0]);
   $('#repaly-commet_post-'+id).focus();
}

function close_iamge(id)
 {
	$('#media-'+id).hide();
	$('#comment_file-'+id).val('');

}
function close_iamge1(id)
 {
	$('#media1-'+id).hide();
	$('#comment_file1-'+id).val('');

}

function genericSocialShare(url){
    window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
    return true;
}


function edit_marketplace_post(id){
    $.ajax({
      url: global_base_url + 'feed/edit_marketplace_post/' + id,
      type: 'GET',
      data: {
      },
      success: function(msg) {
        $('#edit_marketplace_item').html(msg);
        $('#edit_marketplace_items').modal('show');
      }
    });
  }


  var waitForEl = function(selector, callback) {
  if (!jQuery(selector).size()) {
    setTimeout(function() {
      window.requestAnimationFrame(function(){ waitForEl(selector, callback) });
    }, 100);
  }else {
    callback();
  }
};

  function share_market_place_post_chat(id,from,to){
    console.log(id);
     $.ajax({
      url: global_base_url + 'feed/share_market_post_in_chat/' + id +'/'+from+'/'+to,
      type: 'get',
      data: {
      },
      dataType: 'JSON',
      success: function(msg) {
        if(msg.status == 1) {
          var selector = $('#active_chat_bubble_'+ msg.chatid);
          waitForEl('#active_chat_bubble_'+ msg.chatid, function() {
            load_active_chat(msg.chatid);
          });
        }
      }
    }); 
  }


$(document).on('click','.read-more-marketplace',function(){
    	var id = $(this).attr('data-id');
    	$("#msg-market-"+id).toggleClass("read-more");
      if($(this).attr('data-read')=='0')
       {
         $(this).attr('data-read',1);
         $(this).html('<i class="fa fa-angle-double-left"></i> Show Less')
       }
       else{
         $(this).attr('data-read',0);
        $(this).html('Read More <i class="fa fa-angle-double-right"></i> ');
       }

  });